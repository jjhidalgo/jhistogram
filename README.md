# jhistogram

Code to compute histograms using the gsl library.

Usage:

%>> jhistogram data_file histogram_output_file number_of_bins log/uni min max norm weight
